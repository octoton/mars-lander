# Mars lander - coding game

Recreation of the mars lander coding game challenge in rust (graphical interface and solver)
![Alt text](assets/Screenshot.png "Screenshot")

Run a simulation in the console with a 1s time step,
then show it on a window with better interpolation (30fps)

You can use spacebar to switch between video format or scrolling format:
	- video format: show current frame until end of simulation
	- scrolling format: show frame corresponding to the mouse cursor x position inside window (x=50 -> first image, x=max-50 -> last image)
