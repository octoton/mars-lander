//!   `terrain.rs` is there to initialize the lander and the terrain (8 terrains have been defined => same as in CodingGame)
//    Copyright (c) 2020  Octoton <https://gitlab.com/octoton>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ----------------------------- TERRAIN CODE -----------------------------
use super::lander;

/// initialize lander and terrain same as Mars Lander - Episode 1
///
/// # Example
///
/// ```
/// let mut lander = resolver::Lander::new();
/// let terrains = resolver::init_to_terrain_0(&mut lander);
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_0(lander: &mut lander::Lander) -> [[f64; 2]; 7] {
	lander.init_position(4500.0, 2700.0);
	lander.init_fuel(550.0);

	[
		[0.0, 100.0],
		[1000.0, 500.0],
		[1500.0, 1500.0],
		[3000.0, 1000.0],
		[4000.0, 150.0],
		[5500.0, 150.0],
		[6999.0, 800.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 2, terrain 1
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_1(lander: &mut lander::Lander) -> [[f64; 2]; 7] {
	lander.init_position(2500.0, 2700.0);
	lander.init_fuel(550.0);

	[
		[0.0, 100.0],
		[1000.0, 500.0],
		[1500.0, 1500.0],
		[3000.0, 1000.0],
		[4000.0, 150.0],
		[5500.0, 150.0],
		[6999.0, 800.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 2, terrain 2
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_2(lander: &mut lander::Lander) -> [[f64; 2]; 10] {
	lander.init_position(6500.0, 2800.0);
	lander.init_speed(-100.0, 0.0);
	lander.init_fuel(600.0);
	lander.init_rotate(90.0);

	[
		[0.0, 100.0],
		[1000.0, 500.0],
		[1500.0, 100.0],
		[3000.0, 100.0],
		[3500.0, 500.0],
		[3700.0, 200.0],
		[5000.0, 1500.0],
		[5800.0, 300.0],
		[6000.0, 1000.0],
		[6999.0, 2000.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 2, terrain 3
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_3(lander: &mut lander::Lander) -> [[f64; 2]; 7] {
	lander.init_position(6500.0, 2800.0);
	lander.init_speed(-90.0, 0.0);
	lander.init_fuel(750.0);
	lander.init_rotate(90.0);

	[
		[0.0, 100.0],
		[1000.0, 500.0],
		[1500.0, 1500.0],
		[3000.0, 1000.0],
		[4000.0, 150.0],
		[5500.0, 150.0],
		[6999.0, 800.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 2, terrain 4
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_4(lander: &mut lander::Lander) -> [[f64; 2]; 20] {
	lander.init_position(500.0, 2700.0);
	lander.init_speed(100.0, 0.0);
	lander.init_fuel(800.0);
	lander.init_rotate(-90.0);

	[
		[0.0, 1000.0],
		[300.0, 1500.0],
		[350.0, 1400.0],
		[500.0, 2000.0],
		[800.0, 1800.0],
		[1000.0, 2500.0],
		[1200.0, 2100.0],
		[1500.0, 2400.0],
		[2000.0, 1000.0],
		[2200.0, 500.0],
		[2500.0, 100.0],
		[2900.0, 800.0],
		[3000.0, 500.0],
		[3200.0, 1000.0],
		[3500.0, 2000.0],
		[3800.0, 800.0],
		[4000.0, 200.0],
		[5000.0, 200.0],
		[5500.0, 1500.0],
		[6999.0, 2800.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 2, terrain 5
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_5(lander: &mut lander::Lander) -> [[f64; 2]; 20] {
	lander.init_position(6500.0, 2700.0);
	lander.init_speed(-50.0, 0.0);
	lander.init_fuel(1000.0);
	lander.init_rotate(90.0);

	[
		[0.0, 1000.0],
		[300.0, 1500.0],
		[350.0, 1400.0],
		[500.0, 2100.0],
		[1500.0, 2100.0],
		[2000.0, 200.0],
		[2500.0, 500.0],
		[2900.0, 300.0],
		[3000.0, 200.0],
		[3200.0, 1000.0],
		[3500.0, 500.0],
		[3800.0, 800.0],
		[4000.0, 200.0],
		[4200.0, 800.0],
		[4800.0, 600.0],
		[5000.0, 1200.0],
		[5500.0, 900.0],
		[6000.0, 500.0],
		[6500.0, 300.0],
		[6999.0, 500.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 3, terrain 1
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_6(lander: &mut lander::Lander) -> [[f64; 2]; 22] {
	lander.init_position(6500.0, 2600.0);
	lander.init_speed(-20.0, 0.0);
	lander.init_fuel(1000.0);
	lander.init_rotate(45.0);

	[
		[0.0, 450.0],
		[300.0, 750.0],
		[1000.0, 450.0],
		[1500.0, 650.0],
		[1800.0, 850.0],
		[2000.0, 1950.0],
		[2200.0, 1850.0],
		[2400.0, 2000.0],
		[3100.0, 1800.0],
		[3150.0, 1550.0],
		[2500.0, 1600.0],
		[2200.0, 1550.0],
		[2100.0, 750.0],
		[2200.0, 150.0],
		[3200.0, 150.0],
		[3500.0, 450.0],
		[4000.0, 950.0],
		[4500.0, 1450.0],
		[5000.0, 1550.0],
		[5500.0, 1500.0],
		[6000.0, 950.0],
		[6999.0, 1750.0],
	]
}

/// initialize lander and terrain same as Mars Lander - Episode 3, terrain 2
///
/// # Example
///
/// ```
/// look at init_to_terrain_0
/// ```
#[allow(dead_code)]
pub fn init_to_terrain_7(lander: &mut lander::Lander) -> [[f64; 2]; 18] {
	lander.init_position(6500.0, 2000.0);
	lander.init_fuel(1200.0);
	lander.init_rotate(0.0);

	[
		[0.0, 1800.0],
		[300.0, 1200.0],
		[1000.0, 1550.0],
		[2000.0, 1200.0],
		[2500.0, 1650.0],
		[3700.0, 220.0],
		[4700.0, 220.0],
		[4750.0, 1000.0],
		[4700.0, 1650.0],
		[4000.0, 1700.0],
		[3700.0, 1600.0],
		[3750.0, 1900.0],
		[4000.0, 2100.0],
		[4900.0, 2050.0],
		[5100.0, 1000.0],
		[5500.0, 500.0],
		[6200.0, 800.0],
		[6999.0, 600.0],
	]
}
