//!   `Lander` manage lander data and simulate movement
//    Copyright (c) 2020  Octoton <https://gitlab.com/octoton>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License

// ----------------------------- LANDERS CODE -----------------------------
/// Index min and max of an array
#[derive(Default)]
pub struct Index {
	pub min: usize,
	pub max: usize,
}

/// 2d cartesien vector
#[derive(Default, Copy, Clone)]
pub struct Vector {
	pub x: f64,
	pub y: f64,
}

/// 2d euler vector
#[derive(Default)]
pub struct VectorPolar {
	pub r: f64,
	pub theta: f64,
}

impl Vector {
	pub fn convert_to_polar(&self) -> VectorPolar {
		let mut polar = VectorPolar {
			..Default::default()
		};

		polar.r = (self.x * self.x + self.y * self.y).sqrt();

		polar.theta = (self.y).atan2(self.x) / TO_RADIANS;

		polar
	}
}

/// Next (power, action) of a lander
#[derive(Default)]
pub struct Action {
	pub power: i32,
	pub rotate: i32,
}

/// Altitude maximum of the map (y axis)
pub const MAX_ALTITUDE: u32 = 3000;
/// Position maximum of the map (x axis)
pub const MAX_POSITION: u32 = 7000;
/// Maximum rotation change of the lander (in °/s, +/-)
const MAX_ROTATION_DELTA: i32 = 15;
/// Maximum power change of the lander (in (m/s²)/s, +/-)
const MAX_POWER_DELTA: i32 = 1;
/// Conversion from degree to radion (in rad/°)
pub const TO_RADIANS: f64 = 3.14 / 180.0;

/// Lander manage data and simulate movement
#[derive(Default, Copy, Clone)]
pub struct Lander {
	fuel: f64,
	power: f64,
	rotate: f64,
	position: Vector,
	speed: Vector,
}

impl Lander {
	// Public function
	/// Create new lander with all value to default
	pub fn new() -> Lander {
		Lander {
			..Default::default()
		}
	}

	pub fn init_position(&mut self, x:  f64, y: f64) {
		self.position.x = x;

		if y < 0.0 {
			self.position.y = 0.0;
		} else {
			self.position.y = y;
		}
	}

	pub fn init_speed(&mut self, x:  f64, y: f64) {
		self.speed.x = x;
		self.speed.y = y;
	}

	pub fn init_fuel(&mut self, fuel: f64) {
		if fuel < 0.0 {
			self.fuel = 0.0
		} else {
			self.fuel = fuel;
		}
	}

	pub fn init_rotate(&mut self, rotate: f64) {
		self.rotate = rotate;
		self.check_and_correct_rotate();
	}

	pub fn get_position(&self) -> &Vector {
		&self.position
	}

	pub fn get_speed(&self) -> &Vector {
		&self.speed
	}

	pub fn get_fuel(&self) -> f64 {
		self.fuel
	}

	pub fn get_rotate(&self) -> f64 {
		self.rotate
	}

	pub fn get_power(&self) -> f64 {
		self.power
	}

	/// Shutdown lander for landing
	pub fn shutdown(&mut self) {
		self.power = 0.0;
	}

	/// Return if lander has crash or landed in comparaison to the land and the autorized landing area: flat
	/// moreover, lander need to be slower than 20m/s in y and 40m/s in x
	pub fn has_crash(&self, land: &[Vector], flat: &Index) -> bool {
		if self.position.x >= land[flat.min].x
			&& self.position.x <= land[flat.max].x
			&& self.position.y <= land[flat.min].y + 50.0
			&& self.position.y >= land[flat.min].y - 50.0
			&& self.speed.x.abs() <= 40.0
			&& self.speed.y.abs() <= 20.0
			&& self.rotate == 0.0
		{
			return false;
		} else {
			return true;
		}
	}

	/// Return if lander as touch the ground set by `land` or go outside the bound of the map
	pub fn end_of_track(&self, land: &[Vector]) -> bool {
		let land_position = Lander::transpose_position_to_land(self.get_position().x, land);

		if self.position.y <= land_position.y {
			return true;
		} else if self.position.x >= MAX_POSITION as f64 || self.position.x <= 0.0 {
			return true;
		} else if self.position.y >= MAX_ALTITUDE as f64 || self.position.y <= 0.0 {
			return true;
		}

		return false;
	}

	/// Set new power and rotation by taking into account the dynamisme of the lander in the chosen dt 
	pub fn set_action(&mut self, power: i32, rotate: i32, dt: f64) {
		// correction on wanted action because system as limit and latencies
		if self.power + MAX_POWER_DELTA as f64 * dt < power as f64 {
			self.power += MAX_POWER_DELTA as f64 * dt;
		} else if self.power - MAX_POWER_DELTA as f64 * dt > power as f64 {
			self.power -= MAX_POWER_DELTA as f64 * dt;
		} else {
			self.power = power as f64;
		}

		if self.rotate + MAX_ROTATION_DELTA as f64 * dt < rotate as f64 {
			self.rotate += MAX_ROTATION_DELTA as f64 * dt;
		} else if self.rotate - MAX_ROTATION_DELTA as f64 * dt > rotate as f64 {
			self.rotate -= MAX_ROTATION_DELTA as f64 * dt;
		} else {
			self.rotate = rotate as f64;
		}

		self.check_and_correct_rotate();
		self.check_and_correct_power();

		if self.fuel > 0.0 {
			if self.fuel < self.power * dt {
				self.power = self.fuel;
			}
		} else {
			self.power = 0.0;
		}
	}

	/// Move lander by dt seconde by taking into account power and rotate
	pub fn go_next(&mut self, dt: f64) {
		let mut a = Vector { x: 0.0, y: -3.711 };

		self.fuel -= self.power * dt;

		a.x += self.power * (self.rotate * TO_RADIANS).sin();
		a.y += self.power * (self.rotate * TO_RADIANS).cos();

		// Euler-Richardson
		let vxmid = self.speed.x - a.x * 0.5 * dt;
		let vymid = self.speed.y + a.y * 0.5 * dt;

		self.position.x += vxmid * dt;
		self.position.y += vymid * dt;

		self.speed.x -= a.x * dt;
		self.speed.y += a.y * dt;
	}

	/// Print lander's info in the console
	pub fn eprintln(&self, dt: f64) {
		eprintln!(
			"X={}m, Y={}m, HSpeed={}m/s VSpeed={}m/s",
			self.position.x.to_integer(),
			self.position.y.to_integer(),
			self.speed.x.to_integer(),
			self.speed.y.to_integer()
		);

		eprintln!(
			"Fuel={}l, Angle={}°, Power={} ({}.0m/s2), dt: {}s",
			self.fuel, self.rotate, self.power, self.power, dt
		);
	}

	// Private function
	fn check_and_correct_rotate(&mut self) {
		if self.rotate > 90.0 {
			self.rotate = 90.0;
		} else if self.rotate < -90.0 {
			self.rotate = -90.0;
		}
	}

	fn check_and_correct_power(&mut self) {
		if self.power > 4.0 {
			self.power = 4.0;
		} else if self.power < 0.0 {
			self.power = 0.0;
		}
	}

	/// Return the (x,y) position of the land above/below the land in the x axis
	fn transpose_position_to_land(x: f64, land: &[Vector]) -> Vector {
		let mut i = 0;
		while i < land.len() && x > land[i].x {
			i += 1;
		}

		if i < land.len() && i != 0 && i - 1 < land.len() {
			return Vector {
				x: x,
				y: (land[i].y - land[i - 1].y) / (land[i].x - land[i - 1].x) * (x - land[i - 1].x)
					+ land[i - 1].y,
			};
		} else {
			return Vector {
				x: land[land.len() - 1].x,
				y: land[land.len() - 1].y,
			};
		}
	}
}

/// Convert float to integer using ceil for print purposes
pub trait PrintVar {
	fn to_integer(&self) -> i32;
}

impl PrintVar for f64 {
	fn to_integer(&self) -> i32 {
		(self.signum() * self.abs().ceil()) as i32
	}
}
