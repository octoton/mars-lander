use opengl_graphics::GlyphCache;
use opengl_graphics::*;
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;
//use sdl2_window::Sdl2Window;
//use piston_window::PistonWindow;
use glutin_window::GlutinWindow;
use piston::AdvancedWindow;

mod resolver;
use resolver::*;

fn main() {
	let opengl = OpenGL::V3_2;

	//terrains
	let terrains_size = [resolver::MAX_POSITION, resolver::MAX_ALTITUDE];

	let screen_size_x = 1000; //870;
	let screen_size = [
		screen_size_x,
		(screen_size_x as f64 * (terrains_size[1] as f64 / terrains_size[0] as f64)) as u32,
	];

	// -------------------------------------------------------------- INIT --------------------------------------------------------------
	let mut lander = resolver::Lander::new();
	let terrains = resolver::init_to_terrain_4(&mut lander);

	let mut resolver = resolver::Resolver::new();
	resolver.set_land(&terrains);
	let mut is_first_loop = true;
	let mut collision_with_terrains = false;
	let mut has_crash = true;
	let dt = 1.0; //-> simu is correct/same as CodingGame if dt = 1

	let mut lander_history: Vec<resolver::Lander> = Vec::new();
	let time_refresh = 30; //fps
	let update_refresh = 30;
	let time_acceleration = 4.0;
	let mut time = 0.0;

	// calculate all the trajectory space
	lander_history.push(lander);
	lander.eprintln(0.0);
	let mut i = 1;
	while i < resolver::EXPLORATION_SIZE && !collision_with_terrains {
		if is_first_loop {
			is_first_loop = false;
			if lander.get_position().x < resolver.get_land()[resolver.get_flat().min].x {
				resolver.set_direction(resolver::Direction::ToLeft); //1
			} else if lander.get_position().x > resolver.get_land()[resolver.get_flat().max].x {
				resolver.set_direction(resolver::Direction::ToRight); //-1
			}
		}

		if collision_with_terrains == false {
			lander.eprintln(dt);

			resolver.next_cycles(&lander, dt);
			resolver.give_lander_action(&mut lander, dt);

			lander.go_next(dt);
		}

		if collision_with_terrains == false && lander.end_of_track(resolver.get_land()) {
			collision_with_terrains = true;
			lander.eprintln(dt);
			lander.shutdown();

			if lander.has_crash(resolver.get_land(), resolver.get_flat()) {
				has_crash = true;
				eprintln!("END CRASH");
			} else {
				has_crash = false;
				eprintln!("END SUCCESS");
			}
		}

		lander_history.push(lander);
		i += 1;
	}

	// show what was appenning
	lander.eprintln(0.0);

	let ref mut window: GlutinWindow =
		WindowSettings::new("opengl_graphics: text_test", screen_size)
			.exit_on_esc(true)
			.graphics_api(opengl)
			.build()
			.unwrap();

	// dpi big in Glutin but PistonWindow has same bug but doesn't allow me to get the dpi
	// and sdl2 has no bug but as really bad perf on kde (and part of the computer ui freeze
	let dpi = window.ctx.window().get_hidpi_factor();
	let screen_size = [
		(screen_size[0] as f64 / dpi) as u32,
		(screen_size[1] as f64 / dpi) as u32,
	];
	window.set_size(screen_size);

	let terrains_ratio = [
		screen_size[0] as f64 / terrains_size[0] as f64,
		screen_size[1] as f64 / terrains_size[1] as f64,
	];

	let mut glyph_cache =
		GlyphCache::new("assets/FiraSans-Regular.ttf", (), TextureSettings::new()).unwrap();

	let mut gl = GlGraphics::new(opengl);
	let mut events = Events::new(
		EventSettings::new()
			.max_fps(time_refresh)
			.ups(update_refresh),
	);

	let black = [0.0, 0.0, 0.0, 1.0];
	let white = [1.0, 1.0, 1.0, 1.0];
	let red = [0.78, 0.16, 0.16, 1.0];
	let orange = [1.0, 0.62, 0.0, 1.0];
	let green1 = [0.0, 0.53, 0.48, 1.0];
	let green2 = [0.0, 0.63, 0.48, 1.0];
	let mut str = String::new();
	let mut timer = 0.0;

	collision_with_terrains = false;
	resolver = resolver::Resolver::new();

	let lander_history = lander_history;
	resolver.set_land(&terrains);
	resolver.next_cycles(&lander_history[0], dt);
	let mut screen_lander = lander_history[0]; //-> simu is correct if update cycle = 1

	if lander_history[0].get_position().x < resolver.get_land()[resolver.get_flat().min].x {
		resolver.set_direction(resolver::Direction::ToLeft);
	} else if lander_history[0].get_position().x > resolver.get_land()[resolver.get_flat().max].x {
		resolver.set_direction(resolver::Direction::ToRight);
	}

	let mut play = true;
	let mut cursor = [0.0, 0.0];
	i = 0;
	while let Some(e) = events.next(window) {
		if let Some(button) = e.release_args() {
			match button {
				Button::Keyboard(key) => {
					if key == Key::Space {
						play = !play;
						println!(
							"Released keyboard key '{:?}', play: {}, i: {}",
							key, play, i
						);
					}
				}
				Button::Mouse(button) => println!("Released mouse button '{:?}'", button),
				Button::Controller(button) => println!("Released controller button '{:?}'", button),
				Button::Hat(hat) => println!("Released controller hat `{:?}`", hat),
			}
		};

		e.mouse_cursor(|pos| {
			if !play {
				cursor = pos;

				let border = 20.0;
				i = ((pos[0] - border) * (lander_history.len() as f64 / (screen_size[0] as f64 - 2.0 * border))) as usize;

				if i > lander_history.len() - 1 {
					i = lander_history.len() - 1;
					collision_with_terrains = true;
				} else {
					collision_with_terrains = false;
				}
				screen_lander = lander_history[i];
			}
		});

		if let Some(args) = e.update_args() {
			if !collision_with_terrains {
				if play && time >= 1.0 {
					time = 0.0;
					i += 1;

					if i > lander_history.len() - 1 {
						i = lander_history.len() - 1;
						collision_with_terrains = true;
					} else {
						collision_with_terrains = false;
					}

					screen_lander = lander_history[i];
				} else if play {
					time += args.dt * time_acceleration;
				}
			}
		}

		if let Some(args) = e.render_args() {
			//use piston_window::Window;
			//let mut str = String::new();
			//let width = (&window.size().width * dpi) as u32;
			//str.push_str(&width.to_string());

			gl.draw(args.viewport(), |c, g| {
				use graphics::*;

				clear(black, g);

				let dt = ms_to_sec_accelerated(fps_to_ms(time_refresh), time_acceleration); // in secondes with time_acceleration taken into account
				if resolver.get_is_a_crash() {
					draw_lines(
						&c,
						g,
						&screen_size,
						&terrains_ratio,
						resolver.get_path(),
						0.5,
						red,
						(1.0 / (dt / 1.7)) as usize,
					);
				} else {
					draw_lines(
						&c,
						g,
						&screen_size,
						&terrains_ratio,
						resolver.get_path(),
						0.5,
						green1,
						(1.0 / (dt / 1.7)) as usize,
					);
				}

				draw_lines(
					&c,
					g,
					&screen_size,
					&terrains_ratio,
					resolver.get_land(),
					1.0,
					red,
					0,
				);
				draw_dotted_lines(
					&c,
					g,
					&screen_size,
					&terrains_ratio,
					resolver.get_flatter_land(),
					0.5,
					orange,
				);
				draw_lines(
					&c,
					g,
					&screen_size,
					&terrains_ratio,
					&resolver.get_land()[resolver.get_flat().min..resolver.get_flat().max + 1],
					1.5,
					green1,
					0,
				);

				if collision_with_terrains && has_crash {
					draw_lander(&c, g, &screen_size, &terrains_ratio, &screen_lander, red);
				} else if collision_with_terrains && !has_crash {
					draw_lander(&c, g, &screen_size, &terrains_ratio, &screen_lander, green2);
				} else {
					draw_lander(&c, g, &screen_size, &terrains_ratio, &screen_lander, white);
				}

				if collision_with_terrains == false {
					resolver.next_cycles(&lander_history[i], dt);
					if play {
						resolver.give_lander_action(&mut screen_lander, dt);
						screen_lander.go_next(dt);
					}

					str.clear();
					str.push_str("TIME");
					str.push_str("            ");
					timer = i as f64 * 1.0;
					let seconds = timer as u64 % 60;
					let minutes = (timer / 60.0) as u64 % 60;
					str.push_str(&format!("{:02}::{:02}", minutes, seconds));
				}

				draw_lander_dashboard(
					&c,
					g,
					&mut glyph_cache,
					&screen_size,
					&terrains_ratio,
					&screen_lander,
					&str,
					white,
				);
			});
		}
	}
}

fn ms_to_sec_accelerated(ms: f64, accel: f64) -> f64 {
	(ms / 1000.0) * accel
}

fn fps_to_ms(fps: u64) -> f64 {
	1000.0 / (fps as f64)
}

fn draw_lines(
	c: &graphics::Context,
	g: &mut opengl_graphics::GlGraphics,
	screen_size: &[u32],
	lines_ratio: &[f64],
	lines_position: &[resolver::Vector],
	size: f64,
	color: [f32; 4],
	with_dot: usize,
) {
	use graphics::*;
	let mut i = 0;
	let lenght = lines_position.len();

	while lenght > 0 && i < lenght - 1 {
		line(
			color,
			size,
			[
				lines_position[i].x as f64 * lines_ratio[0],
				screen_size[1] as f64 - (lines_position[i].y as f64 * lines_ratio[1]),
				lines_position[i + 1].x as f64 * lines_ratio[0],
				screen_size[1] as f64 - (lines_position[i + 1].y as f64 * lines_ratio[1]),
			],
			c.transform,
			g,
		);

		if i != 0 && with_dot != 0 && i % with_dot == 0 {
			let m = (lines_position[i + 1].y as f64 - lines_position[i].y as f64)
				/ (lines_position[i + 1].x as f64 - lines_position[i].x as f64);

			let theta = (1.0 / (1.0 + m * m).sqrt()).acos() + 3.14 / 2.0;
			let c_prim = c
				.transform
				.trans(
					lines_position[i + 1].x * lines_ratio[0],
					screen_size[1] as f64 - (lines_position[i + 1].y * lines_ratio[1]),
				)
				.rot_rad(theta);

			let depl = 1.8;
			let x1new = -depl;
			let y1new = 0.0;
			let x2new = depl;
			let y2new = 0.0;
			line(color, size, [x1new, y1new, x2new, y2new], c_prim, g);
			//ellipse(color, [ lines_position[i].x as f64 * lines_ratio[0] - diameter_x/2.0, screen_size[1] as f64 - (lines_position[i].y as f64 * lines_ratio[1]) - diameter_y/2.0, diameter_x, diameter_y ], c.transform, g);
		}

		i += 1;
	}
}

fn draw_dotted_lines(
	c: &graphics::Context,
	g: &mut opengl_graphics::GlGraphics,
	screen_size: &[u32],
	lines_ratio: &[f64],
	lines_position: &[resolver::Vector],
	size: f64,
	color: [f32; 4],
) {
	use graphics::*;
	let mut i = 0;

	let mut x1 = lines_position[i].x;
	let mut x2 = 0.0;
	let lenght = lines_position.len();
	while lenght > 0 && i < lenght - 1 {
		let depl = 200.0;
		let m = (lines_position[i + 1].y as f64 - lines_position[i].y as f64)
			/ (lines_position[i + 1].x as f64 - lines_position[i].x as f64);
		let p = lines_position[i].y as f64 - m * lines_position[i].x as f64;
		let sub_depl = (depl * depl / (1.0 + m * m)).sqrt();

		if x1 > lines_position[i].x {
			x1 = x2 + sub_depl / 2.0;
		} else {
			x1 = lines_position[i].x;
		}

		while x1 < lines_position[i + 1].x {
			let y1 = m * x1 + p;

			x2 = x1 + sub_depl / 2.0;
			if x2 > lines_position[i + 1].x {
				x2 = lines_position[i + 1].x;
			}
			let y2 = m * x2 + p;
			line(
				color,
				size,
				[
					x1 * lines_ratio[0],
					screen_size[1] as f64 - (y1 * lines_ratio[1]),
					x2 * lines_ratio[0],
					screen_size[1] as f64 - (y2 * lines_ratio[1]),
				],
				c.transform,
				g,
			);

			x1 += sub_depl;
		}

		i += 1;
	}
}

fn draw_lander(
	c: &graphics::Context,
	g: &mut opengl_graphics::GlGraphics,
	screen_size: &[u32],
	lander_ratio: &[f64],
	lander: &resolver::Lander,
	color: [f32; 4],
) {
	use graphics::*;

	let diameter_x = 140.0 * lander_ratio[0];
	let diameter_y = 140.0 * lander_ratio[1];
	let size_leg_y = 80.0 * lander_ratio[1];
	let gap = 28.0 * lander_ratio[1]; // add little space between foot and ground

	ellipse(
		color,
		[
			lander.get_position().x as f64 * lander_ratio[0] - diameter_x / 2.0,
			screen_size[1] as f64
				- (lander.get_position().y as f64 * lander_ratio[1])
				- (diameter_y + size_leg_y + gap),
			diameter_x,
			diameter_y,
		],
		c.transform,
		g,
	);

	let c_prim = c
		.transform
		.trans(
			lander.get_position().x as f64 * lander_ratio[0],
			screen_size[1] as f64
				- (lander.get_position().y as f64 * lander_ratio[1])
				- diameter_y / 2.0
				- size_leg_y - gap,
		)
		.rot_rad(-lander.get_rotate() as f64 * resolver::TO_RADIANS as f64);
	let diameter_x2 = diameter_x / 1.1;
	let diameter_y2 = diameter_y / 1.1;
	ellipse(
		[0.0, 0.0, 0.0, 1.0],
		[
			0.0 - diameter_x2 / 2.0,
			0.0 - diameter_y2 / 2.0,
			diameter_x2,
			diameter_y2,
		],
		c_prim,
		g,
	);

	let width = 3.0;
	line(
		color,
		width,
		[0.0, diameter_y / 2.0, 0.0, diameter_y / 2.0 + 2.0],
		c_prim,
		g,
	);

	//legs
	let x1 = width;
	let x2 = 1.8 * width;
	let x3 = 2.6 * width;
	let y1 = diameter_y / 2.0 + 2.0;
	let y2 = diameter_y / 2.0 + size_leg_y;
	line(color, 0.5, [x1, y1, x2, y2], c_prim, g);
	line(color, 0.5, [-x1, y1, -x2, y2], c_prim, g);
	//foot
	line(color, 0.5, [x2, y2, x3, y2], c_prim, g);
	line(color, 0.5, [-x2, y2, -x3, y2], c_prim, g);

	if lander.get_power() > 0.0 {
		let width = diameter_x / 2.0;
		let height = 7.5 * lander.get_power() as f64;
		ellipse(
			color,
			[0.0 - width / 4.0, diameter_y / 1.1, width / 2.0, height],
			c_prim,
			g,
		);
		ellipse(
			[0.0, 0.0, 0.0, 1.0],
			[
				0.0 - width / 4.0 + 0.6,
				5.0 + diameter_y / 1.1,
				width / 2.0 - 1.2,
				height - 10.0,
			],
			c_prim,
			g,
		);
	}
}

fn draw_lander_dashboard(
	c: &graphics::Context,
	g: &mut opengl_graphics::GlGraphics,
	glyph_cache: &mut GlyphCache,
	screen_size: &[u32],
	lander_ratio: &[f64],
	lander: &resolver::Lander,
	time: &String,
	color: [f32; 4],
) {
	let text_size = 9;
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		time,
		500.0,
		2500.0,
		text_size,
		color,
	);

	let mut string = String::new();
	string.push_str("ALTITUDE    ");
	string.push_str(&(lander.get_position().y.to_integer()).to_string());
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		&string,
		500.0,
		2350.0,
		text_size,
		color,
	);

	string.clear();
	string.push_str("POSITION    ");
	string.push_str(&(lander.get_position().x.to_integer()).to_string());
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		&string,
		500.0,
		2200.0,
		text_size,
		color,
	);

	string.clear();
	string.push_str("FUEL                             ");
	string.push_str(&(lander.get_fuel() as u64).to_string());
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		&string,
		5000.0,
		2500.0,
		text_size,
		color,
	);

	string.clear();
	string.push_str("HORIZONTAL SPEED    ");
	string.push_str(&(lander.get_speed().x.to_integer()).to_string());
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		&string,
		5000.0,
		2350.0,
		text_size,
		color,
	);

	string.clear();
	string.push_str("VERTICAL SPEED         ");
	string.push_str(&(lander.get_speed().y.to_integer()).to_string());
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		&string,
		5000.0,
		2200.0,
		text_size,
		color,
	);

	string.clear();
	string.push_str("POWER ");
	string.push_str(&(lander.get_power() as i64).to_string());
	string.push_str(", ROTATE ");
	string.push_str(&(lander.get_rotate() as i64).to_string());
	draw_text(
		c,
		g,
		glyph_cache,
		screen_size,
		lander_ratio,
		&string,
		500.0,
		200.0,
		text_size,
		color,
	);
}

fn draw_text(
	c: &graphics::Context,
	g: &mut opengl_graphics::GlGraphics,
	glyph_cache: &mut GlyphCache,
	screen_size: &[u32],
	text_ratio: &[f64],
	string: &String,
	x: f64,
	y: f64,
	size: u32,
	color: [f32; 4],
) {
	use graphics::*;

	text::Text::new_color(color, size)
		.draw(
			&string,
			glyph_cache,
			&DrawState::default(),
			c.transform
				.trans(x * text_ratio[0], screen_size[1] as f64 - y * text_ratio[1]),
			g,
		)
		.unwrap();
}
