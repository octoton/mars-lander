//!    `Resolver` manage what to do next for the lander to land on the designated area
//    Copyright (c) 2020  Octoton <https://gitlab.com/octoton>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ----------------------------- RESOLVER CODE -----------------------------
pub mod lander;
pub mod terrain;
pub use lander::*;
pub use terrain::*;

/// Direction in the x axis (left, rigth, none)
#[derive(PartialEq, Clone, Copy)]
pub enum Direction {
	None = 0,
	ToLeft = 1,
	ToRight = -1,
}

/// Maximum size of the search depth
pub const EXPLORATION_SIZE: usize = 6000;
impl Default for Resolver {
	#[inline]
	fn default() -> Resolver {
		Resolver {
			lander: lander::Lander::new(),
			direction: Direction::None,
			next_lander: lander::Lander::new(),
			chosen_action: lander::Action {
				..Default::default()
			},
			land: Vec::new(),

			flat: lander::Index { min: 0, max: 0 },
			flatter_land: Vec::new(),

			path: Vec::new(),
			is_a_crash: true,

			print_debug: false,
		}
	}
}

pub struct Resolver {
	lander: lander::Lander,
	direction: Direction,
	next_lander: lander::Lander,
	chosen_action: lander::Action,
	land: Vec<lander::Vector>,

	flat: lander::Index,
	flatter_land: Vec<lander::Vector>,

	path: Vec<lander::Vector>,
	is_a_crash: bool,

	print_debug: bool,
}

impl Resolver {
	// Public function
	/// Create new resolver with all value to default
	pub fn new() -> Resolver {
		Resolver {
			..Default::default()
		}
	}

	pub fn get_flat(&self) -> &Index {
		&self.flat
	}

	pub fn get_land(&self) -> &Vec<lander::Vector> {
		&self.land
	}

	pub fn get_is_a_crash(&self) -> bool {
		self.is_a_crash
	}

	pub fn get_path(&self) -> &Vec<lander::Vector> {
		&self.path
	}

	pub fn get_flatter_land(&self) -> &Vec<lander::Vector> {
		&self.flatter_land
	}

	pub fn set_direction(&mut self, direction: Direction) {
		self.direction = direction;
	}

	/// Simulate next cycles until crash/landing/out of bound or end of depth
	pub fn next_cycles(&mut self, real_lander: &lander::Lander, dt: f64) {
		self.lander = *real_lander;
		self.next_lander = self.lander;

		self.path.clear();
		let mut i = 0;
		let mut end_of_track = false;
		while i < EXPLORATION_SIZE && !end_of_track {
			self.path.push(*self.next_lander.get_position());

			self.next_simulation_action(dt);

			self.next_lander.go_next(dt);

			end_of_track = self.next_lander.end_of_track(&self.flatter_land);
			i += 1;
		}

		if end_of_track {
			if self.print_debug {
				eprintln!("    Landing in {} turn, lander infos:", i + 1);
				self.next_lander.eprintln(dt);
				eprintln!("");
			}

			self.path.push(*self.next_lander.get_position());

			self.is_a_crash = self.next_lander.has_crash(&self.land, &self.flat);
		}

		self.set_action(dt);
	}

	/// Public function to give a lander it's next action
	pub fn give_lander_action(&self, lander: &mut lander::Lander, dt: f64) {
		lander.set_action(self.chosen_action.power, self.chosen_action.rotate, dt);
	}

	/// Set land same as `new`, flat as the landing area and flatter_land as authorized position for the lander
	pub fn set_land(&mut self, new: &[[f64; 2]]) -> bool {
		let mut begin_flat = false;
		let mut prev_land = lander::Vector { x: -1.0, y: -1.0 };
		for i in 0..new.len() {
			self.land.push(lander::Vector {
				x: new[i][0],
				y: new[i][1],
			});
			self.flatter_land.push(self.land[i]);

			// found flat land
			if self.land[i].x != prev_land.x && self.land[i].y == prev_land.y {
				if !begin_flat {
					self.flat.min = i - 1;
				}

				begin_flat = true;
			} else if begin_flat {
				self.flat.max = i - 1;
				begin_flat = false;
			}

			prev_land.x = self.land[i].x;
			prev_land.y = self.land[i].y;
		}

		eprintln!(
			"------------------------------- FLAT: {} {}",
			self.flat.min, self.flat.max
		);
		eprintln!(
			"------------------------------- FLAT: {:?}",
			&new[self.flat.min .. self.flat.max + 1]
		);

		// flatter_land.y cannot be less than landing area
		for v in &mut self.flatter_land {
			if v.y < self.land[self.flat.min].y {
				(*v).y = self.land[self.flat.min].y;
			}
		}

		// correct flatter_land.y
		for i in 1..self.flatter_land.len() {
			if self.land[i].x < self.land[self.flat.min].x {
				let mut j = i - 1;
				while j < new.len() && self.flatter_land[j].y < self.land[i].y {
					self.flatter_land[j].y = self.land[i].y;

					if j == 0 {
						break;
					} else {
						j -= 1;
					}
				}
			} else if self.land[i].x > self.land[self.flat.max].x {
				let mut j = i + 1;
				while j < new.len() && self.flatter_land[j].y < self.land[i].y {
					self.flatter_land[j].y = self.land[i].y;
					j += 1;
				}
			}
		}

		true
	}

	// Private function
	/// Transform a speed into an action
	fn from_speed_to_action(
		lander: &lander::Lander,
		v_speed: f64,
		h_speed: f64,
		dt: f64,
	) -> lander::VectorPolar {
		let a = lander::Vector { x: 0.0, y: -3.711 };
		let mut vector = lander::Vector {
			..Default::default()
		};

		vector.y = (lander.get_speed().x - h_speed) / dt - a.x;
		vector.x = (v_speed - lander.get_speed().y) / dt - a.y;

		vector.convert_to_polar()
	}

	/// Generic way to set a new action
	fn next_action(
		lander: &lander::Lander,
		land: &[lander::Vector],
		flat: &lander::Index,
		dt: f64,
	) -> (i32, i32, f64, f64) {
		let mut power;
		let mut rotate;

		let mut v_speed = 0.0;
		let mut h_speed = 0.0;
		if lander.get_position().x >= land[flat.min].x && lander.get_position().x <= land[flat.max].x {
			v_speed = -15.0;
			h_speed = 0.0;
		} else if lander.get_position().x < land[flat.min].x {
			v_speed = 0.0;
			h_speed = 40.0;
		} else if lander.get_position().x > land[flat.max].x {
			v_speed = 0.0;
			h_speed = -40.0;
		}

		let vector_polar = Resolver::from_speed_to_action(lander, v_speed, h_speed, dt);

		power = vector_polar.r as i32;
		rotate = vector_polar.theta as i32;

		// if landing is near
		if lander.get_position().x >= land[flat.min].x
			&& lander.get_position().x <= land[flat.max].x
			&& lander.get_position().y < land[flat.min].y + 200.0
		{
			if lander.get_speed().y > -15.0 {
				power = 3;
			} else {
				power = 4;
			}
			rotate = 0;
		}

		(power, rotate, v_speed, h_speed)
	}

	/// Next action used for simulation
	fn next_simulation_action(&mut self, dt: f64) {
		let (power, rotate, _v_speed, _h_speed) =
			Resolver::next_action(&self.next_lander, &self.land, &self.flat, dt);

		self.next_lander.set_action(power, rotate, dt);
	}

	/// Set real action by taking into account simulation result
	fn set_action(&mut self, dt: f64) {
		let action = Resolver::next_action(&self.lander, &self.land, &self.flat, dt);
		let mut power = action.0;
		let mut rotate = action.1;
		let mut v_speed = action.2;
		let mut h_speed = action.3;

		if self.is_a_crash {
			let mut redo = true;

			if self.next_lander.get_position().x >= self.land[self.flat.min].x
				&& self.next_lander.get_position().x <= self.land[self.flat.max].x
			{
				// inside but v or h are to much or out of bound
				if self.next_lander.get_fuel() == 0.0 {
					power = 0;
					rotate = 0;

					redo = false;
				} else if self.next_lander.get_position().y >= lander::MAX_ALTITUDE as f64 - 50.0 {
					power = 0;
					rotate = 0;

					redo = false;
				} else if self.next_lander.get_speed().y < -20.0 {
					v_speed += 40.0;
					h_speed /= 4.0;
				} else if self.next_lander.get_speed().x.abs() > 40.0 {
					h_speed -= self.direction as i8 as f64 * 20.0;
				}
			} else if self.next_lander.get_fuel() == 0.0 {
				v_speed *= 2.0;
				h_speed *= 2.0;
			} else if (self.direction == Direction::ToLeft
				&& self.next_lander.get_position().x < self.land[self.flat.min].x)
				|| (self.direction == Direction::ToRight
					&& self.next_lander.get_position().x > self.land[self.flat.max].x)
			{
				// too short
				h_speed += self.direction as i8 as f64 * 20.0;
			} else if (self.direction == Direction::ToLeft
				&& self.next_lander.get_position().x > self.land[self.flat.max].x)
				|| (self.direction == Direction::ToRight
					&& self.next_lander.get_position().x < self.land[self.flat.min].x)
			{
				// too much
				h_speed -= self.direction as i8 as f64 * 20.0;
			}

			if redo {
				let vector_polar =
					Resolver::from_speed_to_action(&self.lander, v_speed, h_speed, dt);

				power = vector_polar.r as i32;
				rotate = vector_polar.theta as i32;
			}
		}

		self.chosen_action.power = power;
		self.chosen_action.rotate = rotate;
	}
}
